package Bridge;

public class Cuadrado extends Forma{
    public Cuadrado(Color colorF) {
        super(colorF);
    }

    @Override
    public void colorear() {
        System.out.println("Pintando");
                colorF.pintar();
    }
}
