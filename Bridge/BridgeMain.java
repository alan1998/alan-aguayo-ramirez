package Bridge;

public class BridgeMain {
    public static void main(String [] args){
        Forma circulo = new Circulo(new Rojo());
        circulo.colorear();
        circulo.colorF = new Azul();
        circulo.colorear();
    }
}
