package Bridge;

public class Circulo extends Forma {
    public Circulo(Color colorF){
        super(colorF);
    }

    @Override
    public void colorear() {
        System.out.println("Color: ");
        colorF.pintar();
    }
}
