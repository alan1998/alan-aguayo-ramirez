package Bridge;

public abstract class Forma {
    protected Color colorF;

    public Forma(Color colorF){
        this.colorF = colorF;
    }
    public abstract void colorear();
}
