package Observer;

import java.util.ArrayList;
import java.util.List;

public class Observador {
    private List<Observado> observadores;
    public Observador(){
        super();
        observadores = new ArrayList<Observado>();
    }
    public void agregarSubscriptor(Observado observador){
        observadores.add(observador);
    }

    public void eliminarSubscriptor(Observado observador){
        observadores.remove(observador);
    }
    public void notificarObservadores(){
        for (Observado observador: observadores)observador.notificar();
    }

}
