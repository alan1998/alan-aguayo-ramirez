package Observer;

public interface Observado {
    void notificar();
}
