package Observer;

public class MainObserver {
    public static void main(String args[]){
        Periodico periodico = new Periodico("Actualizacion");
        Subscriptor martin = new Subscriptor("Pedro",periodico);
        periodico.agregarSubscriptor(martin);

        Subscriptor juan = new Subscriptor("Pedro",periodico);
        periodico.agregarSubscriptor(juan);

        periodico.eliminarSubscriptor(juan);

    }

}
