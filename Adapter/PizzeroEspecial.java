package Adapter;

public class PizzeroEspecial extends Pizzero {
    @Override
    public void agregarHarina() {
        System.out.println("Agregarle queso");
    }

    @Override
    public void agregarHuevos() {
        System.out.println("Agregarle Peperoni");
    }

    public void agregarTocino() {
        System.out.println("Agregarle Tocino");
    }
}
