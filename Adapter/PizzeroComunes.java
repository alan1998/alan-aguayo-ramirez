package Adapter;

public class PizzeroComunes extends Pizzero {
    @Override
    public void agregarHarina() {
        System.out.println("Agregarle queso");
    }

    @Override
    public void agregarHuevos() {
        System.out.println("Agregarle Peperoni");
    }
}
