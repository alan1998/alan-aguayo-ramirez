package Adapter;

public class MainAdapter {
    public static void main(String[] args){
        System.out.println("Pizza Comun \n");
        PizzeroComunes pizzaComun = new PizzeroComunes();
        pizzaComun.agregarHarina();
        pizzaComun.agregarHuevos();

        System.out.println("\n Pizza especial \n");
        PizzeroEspecial pizzaEspecial = new PizzeroEspecial();
        pizzaEspecial.agregarHarina();
        pizzaEspecial.agregarHuevos();
        pizzaEspecial.agregarTocino();

        System.out.println("\n Pastel \n");
        Pastel pastel = new Pastel();
        pastel.ponerAzucar();
        pastel.decorar();
    }
}
