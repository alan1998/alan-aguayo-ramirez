package Adapter;

public class PizzeroAdapterPastel extends Pizzero {

    Pastel pastel;
    public PizzeroAdapterPastel() {
       this.pastel = new Pastel();
    }

    @Override
    public void agregarHarina() {
        System.out.println("Agregar harina a pastel");
        this.pastel.ponerAzucar();
    }

    @Override
    public void agregarHuevos() {
        System.out.println("agregar huevos al pastel");
        this.pastel.decorar();
    }
}
